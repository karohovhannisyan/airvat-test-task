import {createSelector} from "reselect";
import {Map, fromJS} from "immutable";

const usersStoreSelector: any = (state: Map<string, any>) => state.get("Users");

const usersSelector: any = createSelector(
    usersStoreSelector, (Users: Map<string, any>) => {
        let users = Users.get("users") && Users.get("users");
        if (users) {
            users = Object.keys(users).map((key) => {
                users[key].id = key;
                return users[key];
            });
        }
        return fromJS(users);
    }
);

const lastVisibleSelector: any = createSelector(
    usersStoreSelector, (Users: Map<string, any>) => Users.get("lastVisible")
);

const prevVisibleSelector: any = createSelector(
    usersStoreSelector, (Users: Map<string, any>) => Users.get("prevVisible")
);

export default state => {
    return {
        users: usersSelector(state),
        lastVisible: lastVisibleSelector(state),
        prevVisible: prevVisibleSelector(state),
    };
};
