import {actions} from "./Reducer";
import {IActions} from "../../types/global";

export function getUsers(lastVisible?: any, prevVisible?: any, orderedBy?: string, search?: Object): IActions {
    return { type: actions.GET_USERS, payload: {lastVisible, prevVisible , orderedBy, search}};
}

export function getUsersSuccess(data: object, lastVisible: object, prevVisible?: Object): IActions {
    return { type: actions.GET_USERS_SUCCESS, payload: {data, lastVisible, prevVisible } };
}

export function getUsersFail(): IActions {
    return { type: actions.GET_USERS_FAIL };
}


