import {call, put, takeLatest} from "redux-saga/effects";
import * as Actions from "./Actions";
import {actions} from "./Reducer";
import Api from "api/Api";

export function* getUsers({ payload }: any): {} {
    try {
        const response: any = yield call(Api.getUsers, payload.lastVisible, payload.prevVisible, payload.orderedBy, payload.search);
        const users: Array<Object> = [];
        const lastVisible: any = response.docs[response.docs.length - 1];
        let prevVisible: any;
        if (payload.lastVisible) {
             prevVisible = payload.lastVisible;
        }
        response.docs.forEach((child) => {
            console.log(child.data())
            users.push({...child.data(), _id: child.id});
        });
        yield put(Actions.getUsersSuccess(users, lastVisible, prevVisible));
    } catch (error) {
        console.error(error)
        yield put(Actions.getUsersFail());
    }
}

function* Saga(): {} {
    yield takeLatest(actions.GET_USERS, getUsers);
}

export default Saga;
