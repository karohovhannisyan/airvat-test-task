import { fromJS } from "immutable";
import {IActionConst} from "../../types/global";

export const actions: IActionConst = {
    GET_USERS: "GET_USERS",
    GET_USERS_SUCCESS: "GET_USERS_SUCCESS",
    GET_USERS_FAIL: "GET_USERS_FAIL",
};

const defaultState: any = fromJS({
    users: null,
    lastVisible: null
});

export default (state = defaultState, {type, payload}) => {
    switch (type) {
        case actions.GET_USERS_SUCCESS:
            return state
                .set("users", payload.data)
                .set("lastVisible", payload.lastVisible)
                .set("prevVisible", payload.prevVisible)

        default:
            return state;
    }
};
