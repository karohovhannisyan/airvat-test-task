import { fromJS } from "immutable";
import {IActionConst} from "../../types/global";

export const actions: IActionConst = {
    ERROR_ACQUIRED: "ERROR_ACQUIRED",
    REMOVE_ERROR: "REMOVE_ERROR",
};

const defaultState: any = fromJS({
    errors: null
});

export default (state = defaultState, {type, payload}) => {
    switch (type) {
        case actions.ERROR_ACQUIRED:
            return state
                .set("errors", fromJS(payload));

        case actions.REMOVE_ERROR:
            return state
                .set("errors", null);

        default:
            return state;
    }
};
