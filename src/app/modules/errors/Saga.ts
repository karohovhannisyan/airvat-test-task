import {put, select, takeLatest} from "redux-saga/effects";
import * as Actions from "./Actions";
import {actions} from "./Reducer";
import {delay} from "redux-saga";
import {errorsSelector} from "./Selector";

export function* throwError(): any {
    yield delay(6000);
    if (yield select(errorsSelector)) {
        yield put(Actions.removeError());
    }
}

function* Saga(): {} {
    yield takeLatest(actions.ERROR_ACQUIRED, throwError);
}

export default Saga;
