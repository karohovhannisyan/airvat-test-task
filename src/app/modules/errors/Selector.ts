import {createSelector} from "reselect";
import {Map} from "immutable";

const errorsStoreSelector: any = (state: Map<string, any>) => state.get("Errors");

export const errorsSelector: any = createSelector(
    errorsStoreSelector, (Errors: Map<string, any>) => Errors.get("errors")
);

export default state => {
    return {
        errors: errorsSelector(state),
    };
};
