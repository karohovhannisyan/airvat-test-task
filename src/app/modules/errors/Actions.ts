import {actions} from "./Reducer";
import {IActions} from "../../types/global";

export function throwError(error: object): IActions {
    return { type: actions.ERROR_ACQUIRED, payload: error };
}
export function removeError(): IActions {
    return { type: actions.REMOVE_ERROR };
}



