import { fromJS } from "immutable";
import {IActionConst} from "../../types/global";

export const actions: IActionConst = {
    SUCCESS_ACQUIRED: "SUCCESS_ACQUIRED",
    REMOVE_SUCCESS: "REMOVE_SUCCESS",
};

const defaultState: any = fromJS({
    successMessage: null
});

export default (state = defaultState, {type, payload}) => {
    switch (type) {
        case actions.SUCCESS_ACQUIRED:
            return state
                .set("successMessage", fromJS(payload));

        case actions.REMOVE_SUCCESS:
            return state
                .set("successMessage", null);

        default:
            return state;
    }
};
