import {put, select, takeLatest} from "redux-saga/effects";
import * as Actions from "./Actions";
import {actions} from "./Reducer";
import {delay} from "redux-saga";
import {successMessageSelector} from "./Selector";
import {removeError} from "../errors/Actions";

export function* throwSuccess(): any {
    yield put(removeError());
    yield delay(6000);
    if (yield select(successMessageSelector)) {
        yield put(Actions.removeSuccess());
    }
}

function* Saga(): {} {
    yield takeLatest(actions.SUCCESS_ACQUIRED, throwSuccess);
}

export default Saga;
