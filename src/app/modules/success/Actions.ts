import {actions} from "./Reducer";
import {IActions} from "../../types/global";

export function throwSuccess(message: string): IActions {
    return { type: actions.SUCCESS_ACQUIRED, payload: message };
}
export function removeSuccess(): IActions {
    return { type: actions.REMOVE_SUCCESS };
}



