import {createSelector} from "reselect";
import {Map} from "immutable";

const errorsStoreSelector: any = (state: Map<string, any>) => state.get("Success");

export const successMessageSelector: any = createSelector(
    errorsStoreSelector, (Success: Map<string, any>) => Success.get("successMessage")
);

export default state => {
    return {
        successMessage: successMessageSelector(state),
    };
};
