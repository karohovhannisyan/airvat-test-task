import getAppConfigurations from "helpers/app/getAppConfigurations";
import {browserHistory, Router} from "react-router";
import {Provider} from "react-redux";
import * as React from "react";

let getAppStore: any;

export default (): React.ReactElement<any> => {

    const {store, routes} = getAppConfigurations();
    getAppStore = () => store;

    return (
        <Provider store={store}>
            <Router history={browserHistory} routes={routes}/>
        </Provider>
    );
};

export { getAppStore };
