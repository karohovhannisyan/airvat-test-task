import {connect} from "react-redux";
import * as React from "react";
import selector from "../services/selector";
import Table from "semantic-ui-react/dist/commonjs/collections/Table";
import Button from "semantic-ui-react/dist/commonjs/elements/Button";
import Icon from "semantic-ui-react/dist/commonjs/elements/Icon";
import {getUsers} from "../modules/user/Actions";
// import * as moment from "moment";
import DateRangePicker from "react-daterange-picker";
import "react-daterange-picker/dist/css/react-calendar.css";
import * as originalMoment from "moment";
import { extendMoment } from "moment-range";
import {TABLE_SORT_HEADER} from "configs/constants";
import {ORDER_TYPES} from "../types/global";
import * as Highlighter from "react-highlight-words";

const moment: any = extendMoment(originalMoment);

class Home extends React.Component<any, any> {
    constructor(props: any) {
        super(props);

        this.state = {
            column: null,
            direction: null,
            search: {},
            isOpen: false,
            value: moment.range(moment(), moment()),
            dateRange: null,
            orderBy: null
        };
    }

    componentDidMount(): void {
        this.props.getUsers();
    }

    search(field: string, value: string): void {
        let state: any = this.state;
        state.search = {};
        state.search[field] = value;
        this.setState(state);
        this.props.getUsers(null, state.search);
    }

    handleSort(clickedColumn: string): void {
        const {orderBy} = this.state;
        if (orderBy) {
            this.setState({
                orderBy: {
                    name: clickedColumn,
                    order: orderBy.name === clickedColumn ? orderBy.order === ORDER_TYPES.ASC ? ORDER_TYPES.DESC : ORDER_TYPES.ASC : ORDER_TYPES.DESC
                }
            }, () => {
                this.props.getUsers(this.state.orderBy);
            });
        } else {
            this.setState({
                orderBy: {
                    name: clickedColumn,
                    order: ORDER_TYPES.DESC
                }
            }, () => {
                this.props.getUsers(this.state.orderBy);
            });
        }
    };

    onDateSelect = (value: any): void => {
        this.setState({ value,
            isOpen: false,
            search: {dateRange: {start: value.start.valueOf(), end: value.end.valueOf()}}
        },
            () => {this.props.getUsers(this.state.orderBy, this.state.search);
        });
    }

    handleOpenDatePicker = (): void => {
        this.setState({ isOpen: !this.state.isOpen });
    }

    renderSelectionValue = (): string => {
        return this.state.search && this.state.search.dateRange ? `${moment(this.state.search.dateRange.start).format("YYYY-MM-DD")}->${moment(this.state.search.dateRange.end).format("YYYY-MM-DD")}` : "";
    }

    renderOrderIcon = (name: string): string => {
        const {orderBy} = this.state;
        if (orderBy) {
            if (orderBy.name === name && orderBy.order === ORDER_TYPES.DESC) {
                return `angle up icon`;
            }
            return `angle down icon`;
        }
        return `angle down icon`;
    }

    render(): JSX.Element {
        const clickedColumn: string = this.state.orderBy && this.state.orderBy.name;
        let { users } = this.props;
        let filteredUser: any = users;
        // filteredUser = filteredUser && this.filter(filteredUser);

        return (
            <div className="main-wrapper">
                <Table sortable celled fixed>
                    <Table.Header>
                        <Table.Row>
                            {
                                TABLE_SORT_HEADER.map((item, index) => (
                                    <Table.HeaderCell
                                        key={index}
                                        onClick={() => this.handleSort(item.value)}
                                    >
                                        {item.name}&nbsp;
                                        <i className={this.renderOrderIcon(item.value)}/>
                                    </Table.HeaderCell>
                                ))
                            }
                        </Table.Row>
                        <Table.Row>
                            <Table.HeaderCell>
                                <div className="ui icon input" style={{maxWidth: "100%"}}>
                                    <i className="search icon"></i>
                                    <input type="text" placeholder="Search..."/>
                                </div>
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                                <div className="ui icon input" style={{maxWidth: "100%"}}>
                                    <i className="search icon"></i>
                                    <input type="text"
                                           value={this.state.search.firstName || ""}
                                           placeholder="Search..."
                                           onChange={(e) => this.search("firstName", e.target.value)}
                                    />
                                </div>
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                                <div className="ui icon input" style={{maxWidth: "100%"}}>
                                    <i className="search icon"></i>
                                    <input type="text"
                                           value={this.state.search.surname || ""}
                                           placeholder="Search..."
                                           onChange={(e) => this.search("surname", e.target.value)}
                                    />
                                </div>
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                                <div className="ui icon input" style={{maxWidth: "100%"}}>
                                    <i className="search icon"></i>
                                    <input
                                        type="text"
                                        placeholder="Search..."
                                        value={this.state.search.email || ""}
                                        onChange={(e) => this.search("email", e.target.value)}/>
                                </div>
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                                <div className="ui icon input" style={{maxWidth: "100%"}}>
                                    <i className="search icon"></i>
                                    <input type="text"
                                           placeholder="Search..."
                                           value={this.state.search.phoneNumber || ""}
                                           onChange={(e) => this.search("phoneNumber", e.target.value)}
                                    />
                                </div>
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                                <div className="ui icon input" style={{maxWidth: "100%"}}>
                                    <i className="search icon"></i>
                                    <input type="text"
                                           placeholder="Search..."
                                           value={this.state.search.residenceCountry || ""}
                                           onChange={(e) => this.search("residenceCountry", e.target.value)}
                                    />
                                </div>
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                                <div className="ui icon input" style={{maxWidth: "100%"}}>
                                    <i className="search icon"></i>
                                    <input type="text"
                                           placeholder="Search..."
                                           onChange={(e) => this.search("residenceCity", e.target.value)}
                                           value={this.state.search.residenceCity || ""}
                                    />
                                </div>
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                                <div className="ui icon input" style={{maxWidth: "99%"}}>
                                    <i className="calendar icon" />
                                    <input type="text"
                                           onClick={this.handleOpenDatePicker}
                                           style={{cursor: "pointer"}}
                                           placeholder="Pick Date"
                                           onChange={() => null} value={this.renderSelectionValue()}/>
                                </div>
                                <div className="date-picker">
                                    {this.state.isOpen && (
                                        <DateRangePicker
                                            value={this.state.value}
                                            onSelect={this.onDateSelect}
                                            singleDateRange={true}
                                        />
                                    )}
                                </div>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {
                            filteredUser && filteredUser.map((user, index) => {
                                return (
                                    <Table.Row key={index}>
                                        <Table.Cell>
                                            <Highlighter
                                                highlightClassName="YourHighlightClass"
                                                searchWords={[this.state.search.id]}
                                                autoEscape={true}
                                                textToHighlight={user.get("_id")}
                                            />
                                        </Table.Cell>
                                        <Table.Cell>
                                            <Highlighter
                                                searchWords={[this.state.search.firstName]}
                                                autoEscape={true}
                                                textToHighlight= {user.getIn(["account", "firstName"])}
                                            />
                                        </Table.Cell>
                                        <Table.Cell>
                                            <Highlighter
                                                searchWords={[this.state.search.surname]}
                                                autoEscape={true}
                                                textToHighlight= {user.getIn(["account", "surname"])}
                                            />
                                        </Table.Cell>
                                        <Table.Cell>
                                            <Highlighter
                                                searchWords={[this.state.search.email]}
                                                autoEscape={true}
                                                textToHighlight= {user.getIn(["account", "email"])}
                                            />
                                        </Table.Cell>
                                        <Table.Cell>
                                            <Highlighter
                                                searchWords={[this.state.search.phoneNumber]}
                                                autoEscape={true}
                                                textToHighlight=  {user.getIn(["account", "phoneNumber"])}
                                            />
                                        </Table.Cell>
                                        <Table.Cell>
                                            <Highlighter
                                                searchWords={[this.state.search.residenceCountry]}
                                                autoEscape={true}
                                                textToHighlight={user.getIn(["account", "residenceCountry"])}
                                            />
                                        </Table.Cell>
                                        <Table.Cell>
                                            <Highlighter
                                                searchWords={[this.state.search.residenceCity]}
                                                autoEscape={true}
                                                textToHighlight={user.getIn(["account", "residenceCity"])}
                                            />
                                        </Table.Cell>
                                        <Table.Cell>{moment(user.get("lastActive")).format("YYYY-MM-DD HH:ss")}</Table.Cell>
                                    </Table.Row>
                                );
                            })
                        }
                    </Table.Body>
                </Table>
                {!users && <div className="ui segment" style={{height: 150}}>
                    <div className="ui active inverted dimmer">
                        <div className="ui text loader">Loading</div>
                    </div>
                    <p></p>
                </div>}
                {users && !users.size && <div className="ui segment" style={{height: 300}}>
                    <div className="ui active inverted dimmer">
                        <img src={"https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png"}/>
                    </div>
                    <p></p>
                </div>}
                {users && Boolean(users.size) && <div>
                    <Button animated disabled={!this.props.prevVisible} onClick={() => this.props.getPrevUsers(this.props.prevVisible)}>
                        <Button.Content visible>Prev</Button.Content>
                        <Button.Content hidden>
                            <Icon name="arrow left" />
                        </Button.Content>
                    </Button>
                    <Button animated onClick={() => this.props.getNextUsers(this.props.lastVisible, this.state.orderBy, this.state.search)}>
                        <Button.Content visible>Next</Button.Content>
                        <Button.Content hidden>
                            <Icon name="arrow right" />
                        </Button.Content>
                    </Button>
                </div>}
            </div>
        );
    }
}

const mapStateToProps: any = state => selector(state);

const mapDispatchToProps: any = dispatch => {
    return {
        getUsers: (orderedBy?: string, search?: Object) => dispatch(getUsers(null, null, orderedBy, search)),
        getNextUsers: (lastVisible: any, orderBy?: string, search?: Object) => dispatch(getUsers(lastVisible, null, orderBy, search)),
        getPrevUsers: (prevVisible) => dispatch(getUsers(null, prevVisible))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
