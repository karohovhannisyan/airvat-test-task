import {LayoutDiv} from "components/index";
import * as React from "react";
import "../../../assets/sass/vendor.scss";
import {LayoutProps} from "../types/layout";

class Layout extends React.Component<LayoutProps, undefined> {
    render(): JSX.Element {
        const {children} = this.props;

        return (
            <>
                <LayoutDiv>
                    {children}
                </LayoutDiv>
            </>
        );
    }
}


export default Layout;
