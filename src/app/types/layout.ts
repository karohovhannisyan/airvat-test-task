export interface LayoutProps {
    children: any;
}

export interface ILayoutDivProps {
    children: any;
    idName?: string;
    style?: any;
}
