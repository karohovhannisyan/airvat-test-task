import {Store} from "react-redux";
import {RouteConfig} from "react-router";

export interface IActions {
    type: string;
    payload?: any;
}

export interface IActionConst {
    [key: string]: string;
}

export interface IAppConfigurations {
    store: Store<any>;
    routes: RouteConfig;
}

export interface IQueryData {
    orderBy?: string;
    equalTo?: string;
}

export enum ORDER_TYPES {
    ASC = "asc",
    DESC = "desc"
}