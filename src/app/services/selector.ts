import usersStoreSelector from "../modules/user/Selector";
import errorsStoreSelector from "../modules/errors/Selector";
import successStoreSelector from "../modules/success/Selector";

export default (state, modules = []) => {
    if (!modules.length) {
        return {
            ...usersStoreSelector(state),
            ...errorsStoreSelector(state),
            ...successStoreSelector(state),
        };
    }

    let stateInProps: object = {};

    if (modules.includes("users")) {
        stateInProps = Object.assign({}, stateInProps, {...usersStoreSelector(state)});
    }

    if (modules.includes("errors")) {
        stateInProps = Object.assign({}, stateInProps, {...errorsStoreSelector(state)});
    }

    if (modules.includes("success")) {
        stateInProps = Object.assign({}, stateInProps, {...successStoreSelector(state)});
    }

    return stateInProps;
};
