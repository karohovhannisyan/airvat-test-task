import AuthSaga from "../modules/user/Saga";
import ErrorHandlerSaga from "../modules/errors/Saga";
import SuccessSaga from "../modules/success/Saga";

export default function*(): any {
    yield [
        AuthSaga(),
        ErrorHandlerSaga(),
        SuccessSaga(),
    ];
};
