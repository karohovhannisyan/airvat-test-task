import {Route, IndexRoute} from "react-router";
import NotFound from "components/NotFound";
import * as Pages from "../pages";
import * as React from "react";
import {HOME_PATH} from "configs/constants";

export default (store) => {
    return (
        <Route path={HOME_PATH} component={Pages.Layout}>
            <IndexRoute component={Pages.Home} />
            <Route path="*" component={NotFound} />
        </Route>
    );
};
