import {combineReducers} from "redux-immutable";
import Users from "../modules/user/Reducer";
import Errors from "../modules/errors/Reducer";
import Success from "../modules/success/Reducer";

export default combineReducers({
    Users,
    Errors,
    Success,
});
