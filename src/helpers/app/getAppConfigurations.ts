import {mainReducer, mainRouter, mainSaga} from "../../app/services";
import configureStore from "../store/configureStore";
import {IAppConfigurations} from "../../app/types/global";

export default (): IAppConfigurations => {
    let configs: any = {
        store: {},
        routes: {}
    };

    configs.store = configureStore(mainReducer, mainSaga);
    configs.routes = mainRouter(configs.store);

    return configs;
};
