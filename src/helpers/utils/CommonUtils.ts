export const NumberArrayGenerator: any = (num) => {
    let result: any = [];

    for (let i: number = 1; i <= num; i++) {
        result.push(i);
    }

    return result;
}
