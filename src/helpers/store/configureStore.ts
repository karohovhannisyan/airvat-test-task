import createSagaMiddleware, {SagaMiddleware} from "redux-saga";
import {applyMiddleware, createStore} from "redux";
import * as createLogger from "redux-logger";
import {Store} from "react-redux";
import {fromJS} from "immutable";

export default (rootReducer: any, rootSaga: any): Store<any> => {

    let store: Store<any>;

    const initialState: any = fromJS({});

    const sagaMiddleware: SagaMiddleware<any> = createSagaMiddleware<any>();

    if (process.env.NODE_ENV !== "production") {
        const logger: any = createLogger({
            collapsed: false
        });

        store = createStore(rootReducer, initialState, applyMiddleware(sagaMiddleware, logger));
    } else {
        store = createStore(rootReducer, initialState, applyMiddleware(sagaMiddleware));
    }

    sagaMiddleware.run(rootSaga);

    return store;
};
