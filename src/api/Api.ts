import * as firebase from "firebase/app";
import "firebase/database";
import "firebase/firestore";
import {fireBaseConfig, PAGE_COUNT_LIMIT, pathsToOrderWithoutAccount} from "configs/constants";
firebase.initializeApp(fireBaseConfig);

const db: any = firebase.firestore();

db.settings({
    timestampsInSnapshots: true
});

const userRef: any = db.collection("users");

export default class Api {
    static getUsers(lastVisible?: any, prevVisible?: any, orderedBy?: {name: string, order: string}, search?: any): Request { //todo
        let request: any = userRef
                       .limit(PAGE_COUNT_LIMIT);
        if (orderedBy) {
            request = request
                .orderBy(pathsToOrderWithoutAccount.includes(orderedBy.name) ? orderedBy.name : `account.${orderedBy.name}`, orderedBy.order);
            // desc
        }

        if (search) {
            Object.keys(search).map((key, index) => {
                if (search[key] && key !== "dateRange") {
                    request = request
                        .orderBy(`account.${key}`)
                        .startAt(search[key].toUpperCase()).endAt(search[key].toUpperCase() + "\uf8ff");
                }
            });

            if (search.dateRange) {
                request = request
                    .where(`lastActive`, ">=", search.dateRange.start)
                    .where(`lastActive`, "<=", search.dateRange.end);
            }
            // request = request
            //     .where(`account.firstName`, search.firstName.toUpperCase());
               // citiesRef.where("state", "==", "CA", "||", "state", "==", "AZ")

        }

        if (lastVisible) {
            request = request
                .startAfter(lastVisible);
        }
        if (prevVisible) {
            request = request
                .endBefore(prevVisible);
        }
        return request.get();
            // .where("account.firstName","==", "LOLA" )
    }

}

