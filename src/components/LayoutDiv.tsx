import * as React from "react";
import {ILayoutDivProps} from "../app/types/layout";

const LayoutDiv: React.StatelessComponent<ILayoutDivProps> = ({children, idName, style}) => {
    return (
        <div className="main-wrapper" id={idName || ""} style={style || {}}>
            <div className="main-wrapper content-wrapper">
                {children}
            </div>
        </div>
    );
};

export default LayoutDiv;
