export const HOME_PATH: string = "/";
export const fireBaseConfig: any = {
    apiKey: "AIzaSyDTrMrVOBvw29R1MLtuyN2RG8fFvFA3R1A",
    authDomain: "airvat-test-task.firebaseapp.com",
    databaseURL: "https://airvat-test-task.firebaseio.com",
    projectId: "airvat-test-task",
    storageBucket: "airvat-test-task.appspot.com",
    messagingSenderId: "980697784779"
};
export const PAGE_COUNT_LIMIT: number = 6;
export const pathsToOrderWithoutAccount: Array<string> = ["lastActive"];

export const TABLE_SORT_HEADER: Array<{name: string, value: string}> = [
    {
        name: "User ID",
        value: "userID"
    },
    {
        name: "First Name",
        value: "firstName"
    },
    {
        name: "Second Name",
        value: "surname"
    },
    {
        name: "Email",
        value: "email"
    },
    {
        name: "Phone Number",
        value: "phoneNumber"
    },
    {
        name: "Residance Country",
        value: "residenceCountry"
    },
    {
        name: "Residance City",
        value: "residenceCity"
    },
    {
        name: "Last Active Date",
        value: "lastActive"
    },
];


