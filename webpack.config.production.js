const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const webpack = require("webpack");
const path = require("path");
const packageJson = require("./package.json");

const METADATA = {
    title: 'sage-admin',
    baseUrl: '/',
    isDevServer: false
};

module.exports = {
    entry: {
        "bundle": ['babel-polyfill', path.join(__dirname, 'dist')]
    },

    output: {
        path: path.join(__dirname, 'production'),
        filename: 'js/[name]-[chunkhash].js',
        chunkFilename: '[name]-[chunkhash].js',
        publicPath: '/'
    },

    resolve: {
        alias: {
            components: path.resolve(__dirname, 'dist/components'),
            params: path.resolve(__dirname, 'dist/configs/params'),
            helpers: path.resolve(__dirname, 'dist/helpers'),
            configs: path.resolve(__dirname, 'dist/configs'),
            sass: path.resolve(__dirname, 'example/sass'),
            apps: path.resolve(__dirname, 'dist/app'),
            api: path.resolve(__dirname, 'dist/api')
        }
    },

    module: {
        rules: [
            {
                test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|eot|ttf|otf)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?name=[name].[ext]&outputPath=css/'
            },
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                exclude: /(node_modules|bower_components)/
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.scss$/,
                exclude: /(node_modules|bower_components)/,
                loader: ExtractTextPlugin.extract({fallback: "style-loader", use: "css-loader!sass-loader"})
            }
        ]
    },

    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                sourceMap: true,
                uglifyOptions: {
                    compress: {warnings: false},
                    mangle: true,
                    sourcemap: true,
                    beautify: false,
                    dead_code: false
                },
            }),
        ],
    },

    performance: {
        hints: false
    },

    plugins: [
        new Dotenv({
            path: './.env', // Path to .env file (this is the default)
            safe: false, // load .env.example (defaults to "false" which does not use dotenv-safe)
            systemvars: true,
            silent: true
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production'),
            'process.env.APP_VERSION': JSON.stringify(packageJson.version.replace(/\./g,''))
        }),
        new CompressionPlugin({
            asset: "[path].gz[query]",
            algorithm: "gzip",
            test: /\.(js|html)$/,
            threshold: 10240,
            minRatio: 0.8
        }),
        new HtmlWebpackPlugin({
            template: path.resolve('./public/index.html'),
            title: METADATA.title,
            chunksSortMode: 'dependency',
            metadata: METADATA,
            filename: 'index.html',
            minify: {
                removeComments: true,
                collapseWhitespace: true
            }
        }),
        new ExtractTextPlugin({filename: "css/style-[chunkhash].css", allChunks: false}),
        new CopyWebpackPlugin([
            {from: 'public/img', to: 'img'},
            {from: 'public/fonts', to: 'fonts'}
        ], {debug: 'debug'})
    ]
};
